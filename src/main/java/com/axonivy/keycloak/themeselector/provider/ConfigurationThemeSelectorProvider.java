package com.axonivy.keycloak.themeselector.provider;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.keycloak.models.ClientModel;
import org.keycloak.models.KeycloakContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.theme.Theme.Type;
import org.keycloak.theme.ThemeSelectorProvider;

public class ConfigurationThemeSelectorProvider implements ThemeSelectorProvider {
	
	private static final String DEFAULT_CLIENT_ID = "klara";
	private static final String CLIENT_ID_QUERY_PARAM_NAME = "client_id";
	
	private KeycloakSession session;
	private Map<String, String> clientIdToThemeName = new HashMap<>();
	
	/**
	 * The provider will be initialized with the Keycloak session and theme configuration for each client
	 * 
	 * @param session
	 * @param clientIdToThemeName
	 */
	public ConfigurationThemeSelectorProvider(KeycloakSession session, Map<String, String> clientIdToThemeName) {
		this.session = session;
		this.clientIdToThemeName = clientIdToThemeName;
    }

	@Override
	public void close() {
		
	}

	/**
	 * Theme name will be selected based on Keycloak session or URL query param `client_id`
	 * 
	 */
	@Override
	public String getThemeName(Type type) {
		Optional<String> clientIdFromQueryString = Optional.ofNullable(session.getContext().getUri().getQueryParameters().getFirst(CLIENT_ID_QUERY_PARAM_NAME));
		String clientId = Optional.ofNullable(session)
				.map(KeycloakSession::getContext)
				.map(KeycloakContext::getClient)
				.map(ClientModel::getClientId)
				.orElse(clientIdFromQueryString.orElse(DEFAULT_CLIENT_ID));
		return clientIdToThemeName.get(clientId);
	}

}
