package com.axonivy.keycloak.themeselector.provider;

import java.util.HashMap;
import java.util.Map;

import org.keycloak.Config.Scope;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.theme.ThemeSelectorProvider;
import org.keycloak.theme.ThemeSelectorProviderFactory;

public class ConfigurationThemeSelectorProviderFactory implements ThemeSelectorProviderFactory {

	/**
	 * The identified theme selector name which will be used to declare in xml configuration file
	 * 
	 */
	private static final String THEME_SELECTOR_NAME = "configurationThemeSelector";
	
	/**
	 * This constant value matches to the configuration property key we define in the configuration xml file
	 * (`standalone.xml`, `standalone-ha.xml`, or `domain.xml`)
	 * 
	 */
	private static final String SUPPORTED_CLIENT_THEMES_KEY = "supported-client-themes";
	private Map<String, String> clientIdToThemeName = new HashMap<>();

	/**
	 * It will return the ThemeSelectorProvider that contains the expected mechanism to select theme
	 * 
	 */
	@Override
	public ThemeSelectorProvider create(KeycloakSession session) {
		return new ConfigurationThemeSelectorProvider(session, clientIdToThemeName);
	}

	/**
	 * When the factory is initialized, it will get the theme configuration for each client from configuration xml file
	 * (`standalone.xml`, `standalone-ha.xml`, or `domain.xml`)
	 * 
	 */
	@Override
	public void init(Scope config) {
		String[] supportedClientThemes = config.get(SUPPORTED_CLIENT_THEMES_KEY).split(",");
		for (String clientThemeName : supportedClientThemes) {
			clientIdToThemeName.put(clientThemeName, config.get(clientThemeName));
		}
	}

	@Override
	public void postInit(KeycloakSessionFactory factory) {
		
	}

	@Override
	public void close() {
		
	}

	@Override
	public String getId() {
		return THEME_SELECTOR_NAME;
	}
	
}
